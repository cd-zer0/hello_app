class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  	def hello
  		render html: "hello,world!"
  	end

  	def goodbye
  		render html: "goodbye"
  	end

  	def spanish_hello
  		render html:  "¡Hola, mundo!"
  		
  	end
end
